import { Request, Response } from 'express';
import response from '../../../libs/response';
import { Admin_VerifySignInOtp_RequestType } from '../../../requests/admin/auth.request';
import {
    AdminFindOne,
    AdminUpdateOneById,
} from '../../../service/admin.service';
import { verifyOTP } from '../../../utils/otp';
import { signJWT } from '../../../utils/manageJWT';
import { USER_TYPES } from '../../../utils/types';

/**
 * Verify OTP for admin sign in.
 * @url     /admin/auth/verify-signin-otp
 * @access  Public
 * @method  POST
 */
const Admin_VerifySignInOtp_Controller = async (
    req: Request,
    res: Response
) => {
    try {
        const {
            body: { email, contactNumber, otp },
        }: Admin_VerifySignInOtp_RequestType = req.body.parsedData;

        const admin = await AdminFindOne({
            $or: [
                {
                    email: {
                        email,
                    },
                },
                {
                    contactNumber: {
                        contactNumber,
                    },
                },
            ],
        });
        if (!admin) {
            throw new Error('Admin not found.');
        }

        verifyOTP(otp, admin.loginOtp.otp, admin.loginOtp.otpSentAt);
        await AdminUpdateOneById(admin.id, {
            loginOtp: {
                otp: null,
                otpSentAt: null,
            },
        });

        const jwtPayload = {
            email: admin.email.email,
            id: admin.id,
            type: USER_TYPES.ADMIN,
        };
        const token = signJWT(jwtPayload);

        return response(req, res, {
            message: 'Admin signed in successfully.',
            token,
        });
    } catch (error) {
        return response(req, res, null, error);
    }
};

export default Admin_VerifySignInOtp_Controller;
