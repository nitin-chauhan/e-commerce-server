import jwt from 'jsonwebtoken';
import env from '../../env';

export const signJWT = (payload: any) => {
    return jwt.sign(payload, env.jwt.secret, {
        expiresIn: env.jwt.expiration,
    });
};

export const verifyJWT = (token: string) => {
    return jwt.verify(token, env.jwt.secret);
};
